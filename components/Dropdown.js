import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity, Dimensions} from 'react-native';
import { MaterialIcons } from '@expo/vector-icons';
export default class App extends React.Component {
	static defaultProps={
	}
	state={
		popup:false
	}
	popupToggle=()=>{
		this.setState({popup:!this.state.popup});
	}
	componentDidMount(){
		this.props.render(this.dd(this.props.current))
	}
	dd=(current)=>{
		return <TouchableOpacity 
							style={{flexDirection:'row', alignItems: 'center',}}
							onPress={this.popupToggle}
							>
						<Text>{this.props.lists[current].label}</Text>
						<MaterialIcons name="arrow-drop-down" size={24}/>
					</TouchableOpacity>
	}
	popup=()=>{
		if(this.state.popup){
		return (
		<View style={styles.popupContainer}>
			<View style={styles.popup}>
				{
					this.props.lists.map((item, index)=>{
						return (
						<TouchableOpacity 
								key={index} 
								onPress={()=>{this.props.onChange(index);this.popupToggle();this.props.render(this.dd(index))}}>
							<Text>{item.label}</Text>
						</TouchableOpacity>);
					})
				}
			</View>
		</View>);
		}
	}
  render() {
		let {lists, current}	= this.props
    return (
      <View>
				{this.popup()}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {},
	popupContainer:{
		position:'absolute',
		top:0,
		backgroundColor:'rgba(0,0,0,0.5)',
		zIndex:9,
		width:Dimensions.get('window').width,
		height:Dimensions.get('window').height,
		paddingLeft:10,
		paddingRight:10,
		justifyContent: 'center',
		width:'100%'
	},
	popup:{
		backgroundColor:'#ffffff',
    borderRadius:8, 
     shadowColor: "#7c7c7c",
     shadowOffset: {
       width: 0,
       height: 5,
     },
		shadowOpacity: 0.2,
		shadowRadius: 8,
		elevation: 10,
		paddingTop:22,
		paddingLeft:20,
		paddingRight:20,
		paddingBottom:23,
		marginTop:16,
		alignItems: 'center',
    justifyContent: 'center',
	}
});
