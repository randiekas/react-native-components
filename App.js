import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Dropdown from './components/Dropdown'
export default class App extends React.Component {
  state = {lists:[
              {value:1, label:"satu"},
              {value:2, label:"dua"},
            ],
            current:0,
            dd:<View/>
          }
  render() {
    let {lists, current, dd} = this.state
    return (
      <View style={styles.container}>
        <Dropdown 
          lists={lists} 
          current={current} 
          onChange={(current)=>{this.setState({current:current})}}
          render={(element)=>{this.setState({dd:element})}}/>
          {dd}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    marginTop:10
    //alignItems: 'center',
    //justifyContent: 'center',
  },
});
